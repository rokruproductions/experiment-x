package com.rokru.experiment_x.level;

import com.rokru.experiment_x.graphics.Render;
import com.rokru.experiment_x.level.tile.Tile;

public class Level {
	
	protected String[] tiles;
	protected int width, height;
	protected int[] tilePixels;
	
	public Level(int width, int height) {
		this.width = width;
		this.height = height;
		tiles = new String[width * height];
		generateLevel();
	}
	
	public Level(String path) {
		loadLevel(path);
		generateLevel();
	}

	protected void loadLevel(String path) {}

	protected void generateLevel() {}
	
	public void update() {
	}
	
	private void time() {
	}
	
	public void render(int xScroll, int yScroll, Render screen) {
		screen.setOffset(xScroll, yScroll);
		int x0 = xScroll >> 4;
		int x1 = (xScroll + screen.width + 16) >> 4;
		int y0 = yScroll >> 4;
		int y1 = (yScroll + screen.height + 16) >> 4;
		
		for (int y = y0; y < y1; y++) {
			for (int x = x0; x < x1; x++) {
				getTile(x, y).render(x, y, screen);
			}
		}
	}
	
	public Tile getTile(int x, int y) {
		if (x < 0 || y < 0 || x >= width || y >= height) return Tile.voidTile;
		else return Tile.getTileFromColorID(tilePixels[x+y*width]);
	}
	
	public int getLevelWidth(){
		return width;
	}
	
	public int getLevelHeight(){
		return height;
	}
	
}
